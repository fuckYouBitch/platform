package com.bigdata.platform.user.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.bigdata.platform.user.entity.UserEntity;
import com.bigdata.platform.user.service.UserService;

@RestController
@RequestMapping("login")
public class LoginController {

	private static final Logger log = Logger.getLogger(LoginController.class);
	@Autowired
	private UserService userService;

	@RequestMapping(value = "login", method = RequestMethod.POST)
	public String findStuInfo(@RequestBody Map<String, String> paramMap) throws Exception {
		String username = paramMap.get("username");
		String password = paramMap.get("password");
		log.info(username + "..............." + password);
		String str = "";
		try {
			Map<String, String> map = new HashMap<String, String>();
			map.put("code", "0");
			map.put("data", "登录成功");
			str = JSON.toJSONString(map);
		} catch (Exception e) {
			log.error("asdasdasd ");
			e.printStackTrace();
			str = "GG";
		}
		return str;
	}

	/**
	 * 
	 * @param paramMap
	 * @return
	 */
	@RequestMapping(value = "queryAllUserList", method = RequestMethod.POST)
	@ResponseBody
	public String queryAllUserList(@RequestBody Map<String, String> paramMap) {
		log.info("查询所有用户列表...............开始" + paramMap);
		String returnStr = "";
		Map<String, Object> retMap = new HashMap<String, Object>();
		try {
			UserEntity user = JSON.parseObject(JSON.toJSONString(paramMap), UserEntity.class);
			List<UserEntity> list = userService.queryAllUserList(user);

			retMap.put("code", "0");
			retMap.put("results", list);
			returnStr = JSON.toJSONString(retMap);
		} catch (Exception e) {
			log.error("查询学生信息异常。。。");
			e.printStackTrace();
			retMap.put("code", "100100");
			retMap.put("errmsg", "查询所有用户列表错误");
			returnStr = JSON.toJSONString(retMap);
		}
		return returnStr;
	}
}
