package com.bigdata.platform.user.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bigdata.platform.user.dao.UserDao;
import com.bigdata.platform.user.entity.UserEntity;

/**
 * 用户服务层
 * 
 * @author Pomoeii
 *
 */
@Service
public class UserService {
	@Autowired
	private UserDao userDao;

	/**
	 * 查询所有用户列表
	 * 
	 * @param user
	 *            用户实体类
	 * @return List<UserEntity> 用户列表
	 */
	public List<UserEntity> queryAllUserList(UserEntity user) {
		return userDao.queryAllUserList(user);
	}
}
