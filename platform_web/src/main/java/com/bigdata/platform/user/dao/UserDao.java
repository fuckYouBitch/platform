package com.bigdata.platform.user.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.bigdata.platform.user.entity.UserEntity;

/**
 * 
 * @author Pomoeii
 *
 */
@Mapper
public interface UserDao {

	/**
	 * 查询所有用户列表
	 * 
	 * @param user
	 *            用户实体类
	 * @return List<UserEntity> 用户列表
	 */
	public List<UserEntity> queryAllUserList(UserEntity user);
}
