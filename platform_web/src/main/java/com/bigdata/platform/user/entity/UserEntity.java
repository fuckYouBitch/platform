package com.bigdata.platform.user.entity;

import org.springframework.stereotype.Component;

/**
 * 用户实体类
 * 
 * @author Pomoeii
 *
 */
@Component
public class UserEntity {

	private Integer userId; // ID
	private String userCode; // 员工编号
	private String username; // 用户名
	private String password; // 密码

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
